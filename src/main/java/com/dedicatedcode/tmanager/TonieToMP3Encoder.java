package com.dedicatedcode.tmanager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

@Service
public class TonieToMP3Encoder implements Reencoder {
    private static final Logger log = LoggerFactory.getLogger(TonieToMP3Encoder.class);
    private final boolean enabled;
    private final String commandPreset;

    public TonieToMP3Encoder(@Value("${automation.media.reencode.enabled:false}") boolean enabled, @Value("${automation.media.reencode.presets.tonie:}") String commandPreset) {
        this.enabled = enabled;
        this.commandPreset = commandPreset;

        if (enabled) {
            verifyFFMpegBinary();
            log.info("Enabled reencoding with preset [{}] for tonie", commandPreset);
        } else {
            log.info("Re-encoding is disabled. To enable it set \"automation.media.reencode.enabled=true\"");
        }
    }

    private void verifyFFMpegBinary() {
        try {
            ProcessBuilder builder = new ProcessBuilder("ffmpeg", "-v");
            builder.redirectErrorStream(true);
            builder.start().waitFor(1, TimeUnit.SECONDS);
            log.debug("ffmpeg found successfully");
        } catch (InterruptedException | IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public File reencode(File input) {
        if (!enabled) {
            return input;
        }
        try {

            File tmpOutput = File.createTempFile(UUID.randomUUID().toString(), ".mp3");

            List<String> command = createCommand(input, tmpOutput);
            ProcessBuilder builder = new ProcessBuilder(command);
            builder.directory(input.getParentFile());
            builder.redirectErrorStream(true);
            Process pro = builder.start();
            if (!pro.waitFor(120, TimeUnit.SECONDS)) {
                log.warn("Process timed out with message: {}", new String(pro.getInputStream().readAllBytes()));
                throw new RuntimeException("Process timed out");
            }
            if (pro.exitValue() != 0) {
                log.warn("Unable to re-encode file [{}]. Will return original file.", input);
            } else {
                log.debug("Re-encoding file [{}] successfully", input);
                String newFilename = extractName(input);
                Files.move(tmpOutput.toPath(), new File(input.getParentFile(), newFilename).toPath(), StandardCopyOption.REPLACE_EXISTING);
                if (!input.getName().equals(newFilename)) {
                    input.delete();
                }
                return new File(input.getParentFile(), newFilename);
            }
        } catch (IOException | InterruptedException e) {
            throw new RuntimeException(e);
        }
        return null;
    }

    private List<String> createCommand(File input, File output) throws IOException {
        String[] parts = this.commandPreset.split("\\s");
        for (int i = 0; i < parts.length; i++) {
            String part = parts[i];
            if (part.equalsIgnoreCase("{in}")) {
                parts[i] = input.getAbsolutePath();
            }
            if (part.equalsIgnoreCase("{out}")) {
                parts[i] = output.getAbsolutePath();
            }
        }
        return Arrays.asList(parts);
    }

    private String extractName(File file) {
        String oldName = file.getName();
        if (oldName.contains(".")) {
            return oldName.substring(0, oldName.lastIndexOf(".")) + ".mp3";
        } else {
            return file.getName();
        }
    }
}
