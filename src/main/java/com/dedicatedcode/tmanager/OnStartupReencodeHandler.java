package com.dedicatedcode.tmanager;

import com.dedicatedcode.tmanager.model.MediaCodecValidator;
import com.dedicatedcode.tmanager.model.collection.MediaJdbcService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.event.ApplicationStartedEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;


@Service
@ConditionalOnProperty(name = "automation.media.reencode.upgrade-existing", havingValue = "true")
public class OnStartupReencodeHandler implements ApplicationListener<ApplicationStartedEvent> {
    private static final Logger log = LoggerFactory.getLogger(OnStartupReencodeHandler.class);
    private final boolean enabled;
    private final Reencoder reencoder;
    private final MediaJdbcService mediaJdbcService;
    private final MediaCodecValidator validator;

    public OnStartupReencodeHandler(@Value("${automation.media.reencode.enabled}") boolean enabled, Reencoder reencoder, MediaJdbcService mediaJdbcService, MediaCodecValidator validator) {
        this.enabled = enabled;
        this.reencoder = reencoder;
        this.mediaJdbcService = mediaJdbcService;
        this.validator = validator;
    }

    @Override
    public void onApplicationEvent(ApplicationStartedEvent event) {
        if (!enabled) {
            log.info("Automation media reencoding is disabled.");
            return;
        }
        new Thread(() -> {
            log.info("Starting up re-encoding on all collections");
            mediaJdbcService.getAllCollections()
                    .stream().flatMap(mc -> mc.getMediaList().stream())
                    .forEach(m -> {
                        File sourceFile = new File(m.getFileLocation());
                        try {
                            if (validator.needsReEncoding(sourceFile)) {
                                File reencoded = reencoder.reencode(sourceFile);
                                mediaJdbcService.update(m.withFileLocation(reencoded.getAbsolutePath()));
                                if (validator.needsReEncoding(reencoded)) {
                                    log.warn("Re-encoded file [{}] failed validation check. This will lead to additional re-encoding. Please match your encoding settings with your validation settings.", reencoded.getName());
                                }
                            }
                        } catch (IOException e) {
                            throw new RuntimeException(e);
                        }
                    });
            log.info("Finished up re-encoding on all collections");
        }
        ).start();

    }
}
