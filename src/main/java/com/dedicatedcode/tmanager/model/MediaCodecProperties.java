package com.dedicatedcode.tmanager.model;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "automation.media.reencode.validation")
public class MediaCodecProperties {
    private String desiredFormat;
    private double maxSizePerMinute;
    private int maxChannels;

    public String getDesiredFormat() {
        return desiredFormat;
    }

    public void setDesiredFormat(String desiredFormat) {
        this.desiredFormat = desiredFormat;
    }

    public double getMaxSizePerMinute() {
        return maxSizePerMinute;
    }

    public void setMaxSizePerMinute(double maxSizePerMinute) {
        this.maxSizePerMinute = maxSizePerMinute;
    }

    public int getMaxChannels() {
        return maxChannels;
    }

    public void setMaxChannels(int maxChannels) {
        this.maxChannels = maxChannels;
    }
}
