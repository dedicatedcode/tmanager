package com.dedicatedcode.tmanager.model.upload;

import com.dedicatedcode.tmanager.UploadManager;
import com.dedicatedcode.tmanager.model.Media;
import com.dedicatedcode.tmanager.model.collection.MediaCollection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;
import rocks.voss.toniebox.TonieHandler;
import rocks.voss.toniebox.beans.toniebox.Chapter;
import rocks.voss.toniebox.beans.toniebox.CreativeTonie;
import rocks.voss.toniebox.beans.toniebox.Household;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

@Service
@ConditionalOnProperty("upload.handler.tonie.id")
public class TonieUploadManager implements UploadManager {
    private static final Logger log = LoggerFactory.getLogger(TonieUploadManager.class);
    private final String username;
    private final String password;

    private final String tonieIdentifier;

    private final Executor executor = Executors.newSingleThreadExecutor();

    private final Map<String, TonieUploadJob> jobs = Collections.synchronizedMap(new LinkedHashMap<>() {
        @Override
        protected boolean removeEldestEntry(Map.Entry<String, TonieUploadJob> eldest) {
            return size() > 100;
        }
    });

    public TonieUploadManager(@Value("${upload.handler.tonie.username}") String username,
                              @Value("${upload.handler.tonie.password}") String password,
                              @Value("${upload.handler.tonie.id}") String tonieIdentifier) {
        this.username = username;
        this.password = password;
        this.tonieIdentifier = tonieIdentifier;
    }

    @Override
    public String schedule(List<MediaCollection> mediaCollections) {
        TonieUploadJob uploadJob = new TonieUploadJob(mediaCollections, username, password, tonieIdentifier);
        log.debug("created new upload job with identifier [{}] for collection [{}]", uploadJob.getJobIdentifier(), mediaCollections.stream().map(MediaCollection::getId).map(UUID::toString).collect(Collectors.joining(",")));
        this.executor.execute(uploadJob);
        this.jobs.put(uploadJob.getJobIdentifier(), uploadJob);
        return uploadJob.getJobIdentifier();
    }

    @Override
    public Optional<UploadStatus> getStatus(String uploadIdentifier) {
        return Optional.ofNullable(this.jobs.get(uploadIdentifier)).map(job -> new UploadStatus(job.state));
    }

    private static class TonieUploadJob implements Runnable {
        private static final int SLEEP_IN_SECONDS = 10;
        private JobState state = JobState.CREATED;
        private final List<MediaCollection> target;
        private final String username;
        private final String password;
        private final String tonieId;
        private final String houseHoldId;
        private final String jobIdentifier;
        private final List<TimestampedMessage> messages = new ArrayList<>();
        private final long created;
        private long startedAt;
        private long finishedAt;

        private final int maxWaitForTranscoding = 60*5;

        private TonieUploadJob(List<MediaCollection> target, String username, String password, String tonieIdentifier) {
            this.target = target;
            this.username = username;
            this.password = password;
            this.tonieId = tonieIdentifier.substring(tonieIdentifier.indexOf("/") + 1);
            this.houseHoldId = tonieIdentifier.substring(0, tonieIdentifier.indexOf("/"));
            this.jobIdentifier = UUID.randomUUID().toString();
            this.created = System.currentTimeMillis();
            log("Upload-Job created");
            log("Household [" + houseHoldId + "]");
            log("Kreativ-Tonie [" + tonieId + "]");
        }

        private void log(String m) {
            log.debug(m);
            this.messages.add(new TimestampedMessage(System.currentTimeMillis(), m));
        }

        private void error(String m) {
            log.error(m);
            this.messages.add(new TimestampedMessage(System.currentTimeMillis(), m));
        }

        private void success(String m) {
            log.info(m);
            this.messages.add(new TimestampedMessage(System.currentTimeMillis(), m));
        }

        @Override
        public void run() {
            this.startedAt = System.currentTimeMillis();
            try {
                log("Upload-Job started.");
                state = JobState.RUNNING;
                TonieHandler tonieHandler = new TonieHandler();
                log("Trying to login with given credentials.");
                tonieHandler.login(username, password);
                success("Login successful, fetching households now.");
                List<Household> households = tonieHandler.getHouseholds();
                Household household = households.stream().filter(h -> h.getId().equals(houseHoldId)).findFirst().orElseThrow(() -> new InvalidConfigurationException("upload.handler.tonie.id", "Unable to find household with given id."));
                success("found household [" + household.getName() + "], loading Kreativ-Tonie now.");
                List<CreativeTonie> allCreativeTonies = tonieHandler.getCreativeTonies(household);
                log("found [" + allCreativeTonies.size() + "] number of associated tonies");
                CreativeTonie creativeTonie = allCreativeTonies.stream().filter(ct -> ct.getId().equals(tonieId)).findFirst().orElseThrow(() -> new InvalidConfigurationException("upload.handler.tonie.id", "Unable to find household with given id."));
                success("found Kreativ-Tonie");
                log("clearing current content");
                Arrays.asList(creativeTonie.getChapters()).forEach(creativeTonie::deleteChapter);
                success("all old chapters removed");
                for (MediaCollection collection : target) {
                    for (int i = 0; i < collection.getMediaList().size(); i++) {
                        Media current = collection.getMediaList().get(i);
                        log.debug("uploading [{}] to tonie [{}]", current.getId(), creativeTonie.getId());
                        log("starting to upload chapter[" + (i + 1) + "/" + collection.getMediaList().size() + "]");
                        creativeTonie.uploadFile(current.getName(), current.getFileLocation());
                        success("uploaded chapter [" + current.getName() + "]");
                    }
                }
                creativeTonie.commit();
                int currentSecond = 0;
                do {
                    creativeTonie.refresh();
                    log("waiting for all transcoding to be done. Since ["+currentSecond+"] seconds.");
                    currentSecond += SLEEP_IN_SECONDS;
                    if (currentSecond >= maxWaitForTranscoding) {
                        throw new IOException("Max wait time for transcoding reached");
                    }
                    Thread.sleep(SLEEP_IN_SECONDS * 1000);
                } while ((Arrays.stream(creativeTonie.getChapters()).anyMatch(Chapter::isTranscoding)));

                tonieHandler.disconnect();
                state = JobState.FINISHED;

            } catch (InterruptedException | IOException ex) {
                error("Error occurred while uploading media to tonie -> " + ex.getMessage());
                log.error("Error while uploading media to tonie", ex);

            } catch (InvalidConfigurationException ex) {
                state = JobState.FAILED;
                error(ex.getMessage());
                log.error("Invalid configuration detected for key [" + ex.getKey() + "]", ex);
            }
            finishedAt = System.currentTimeMillis();
            log.info("Job [{}] finished after [{}]ms", jobIdentifier, this.finishedAt - this.startedAt);
        }

        public JobState getState() {
            return state;
        }

        public String getJobIdentifier() {
            return jobIdentifier;
        }

        private record TimestampedMessage(long timestamp, String message) {}
    }

    private static final class InvalidConfigurationException extends RuntimeException {
        private final String key;

        private InvalidConfigurationException(String key, String message) {
            super(message);
            this.key = key;
        }

        public String getKey() {
            return key;
        }
    }
}
