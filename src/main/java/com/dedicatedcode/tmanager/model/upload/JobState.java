package com.dedicatedcode.tmanager.model.upload;

public enum JobState {
    FAILED,
    RUNNING,
    CREATED, FINISHED
}
