package com.dedicatedcode.tmanager.model.collection;

import com.dedicatedcode.tmanager.model.Media;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

public final class MediaCollection {
    private final UUID id;
    private final String name;
    private final List<Media> mediaList;
    private final boolean system;

    public MediaCollection(UUID id, String name, List<Media> mediaList) {
        this(id, name, mediaList, false);
    }

    MediaCollection(UUID id, String name, List<Media> mediaList, boolean system) {
        this.id = id;
        this.name = name;
        this.mediaList = mediaList;
        this.system = system;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MediaCollection that = (MediaCollection) o;
        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public MediaCollection append(Media media) {
        List<Media> newMedia = new ArrayList<>(mediaList);
        newMedia.add(media);
        return new MediaCollection(id, name, newMedia, system);
    }

    public UUID getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public List<Media> getMediaList() {
        return mediaList;
    }

    public boolean isSystem() {
        return system;
    }

    @Override
    public String toString() {
        return "MediaCollection[" +
               "id=" + id + ", " +
               "name=" + name + ", " +
               "mediaList=" + mediaList + ", " +
               "system=" + system + ']';
    }

    public MediaCollection withName(String name) {
        return new MediaCollection(id, name, mediaList, system);
    }

}
