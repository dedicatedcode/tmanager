package com.dedicatedcode.tmanager.model.collection;

import com.dedicatedcode.tmanager.model.Media;
import com.dedicatedcode.tmanager.model.tag.Tag;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class MediaJdbcService {

    private final JdbcTemplate jdbcTemplate;

    public MediaJdbcService(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public List<MediaCollection> getAllCollections() {
        return this.jdbcTemplate.query("SELECT * FROM collection", this::mapMediaCollection);
    }


    public Optional<MediaCollection> getCollectionBy(UUID id) {
        List<MediaCollection> mediaCollections = this.jdbcTemplate.query("SELECT * FROM collection WHERE id = ?", this::mapMediaCollection, id.toString());
        return mediaCollections.stream().findAny();
    }

    public Optional<MediaCollection> getCollectionBy(String name) {
        List<MediaCollection> mediaCollections = this.jdbcTemplate.query("SELECT * FROM collection WHERE name = ?", this::mapMediaCollection, name);
        return mediaCollections.stream().findAny();
    }

    public List<MediaCollection> getCollectionsBy(Tag tag) {
        return this.jdbcTemplate.query("SELECT * FROM collection WHERE id IN (SELECT collection_id FROM collections_tags WHERE tag_id = ?)", this::mapMediaCollection, tag.getIdentifier());
    }


    public Optional<MediaCollection> getCollectionBy(Media media) {
        List<MediaCollection> mediaCollections = this.jdbcTemplate.query("SELECT * FROM collection WHERE id IN (SELECT collection_id FROM media WHERE id = ?)", this::mapMediaCollection, media.getId());
        return mediaCollections.stream().findAny();
    }


    public void create(MediaCollection mediaCollection) {
        this.jdbcTemplate.update("INSERT INTO collection(id, name) VALUES (?,?)", mediaCollection.getId().toString(), mediaCollection.getName());
    }

    public void update(MediaCollection mediaCollection) {
        this.jdbcTemplate.update("UPDATE collection SET name = ? WHERE id = ?", mediaCollection.getName(), mediaCollection.getId().toString());
    }

    public Optional<Media> getMedia(UUID id) {
        return this.jdbcTemplate.query("SELECT * FROM media WHERE id = ?", MediaJdbcService::mapMedia, id.toString()).stream().findAny();
    }

    public void saveMedia(MediaCollection collection, Media media) {
        this.jdbcTemplate.update("DELETE FROM media WHERE id = ?", media.getId());
        this.jdbcTemplate.update("INSERT INTO media (id, name, storage_path, collection_id, duration, sort_order) VALUES (?,?,?,?,?,?)",
                media.getId().toString(),
                media.getName(),
                media.getFileLocation(),
                collection.getId().toString(),
                media.getDuration(),
                collection.getMediaList().indexOf(media));
    }

    public MediaCollection getDefaultCollection() {
        List<MediaCollection> mediaCollections = this.jdbcTemplate.query("SELECT * FROM collection WHERE is_default", this::mapMediaCollection);
        return mediaCollections.stream().findAny().orElseThrow(IllegalStateException::new);
    }


    private static Media mapMedia(ResultSet rs, int rowNum) throws SQLException {
        return new Media(UUID.fromString(rs.getString("id")), rs.getString("name"), rs.getString("storage_path"), rs.getLong("duration"));
    }

    private MediaCollection mapMediaCollection(ResultSet rs, int rowNum) throws SQLException {

        UUID id = UUID.fromString(rs.getString("id"));
        List<Media> mediaList = this.jdbcTemplate.query("SELECT * FROM media WHERE collection_id = ? ORDER BY sort_order",
                (rs1, rowNum1) -> new Media(UUID.fromString(rs1.getString("id")), rs1.getString("name"), rs1.getString("storage_path"), rs1.getLong("duration")), id.toString());
        return new MediaCollection(id, rs.getString("name"), mediaList, rs.getBoolean("is_default"));
    }

    public void update(Media media) {
        this.jdbcTemplate.update("UPDATE media SET name = ?, storage_path = ? WHERE id = ?", media.getName(), media.getFileLocation(), media.getId().toString());
    }

    public void delete(Media media) {
        this.jdbcTemplate.update("DELETE FROM media WHERE id = ?", media.getId().toString());
    }

    public void delete(MediaCollection collection) {
        this.jdbcTemplate.update("DELETE FROM collections_tags where collection_id = ?", collection.getId().toString());
        this.jdbcTemplate.update("DELETE FROM collection WHERE id = ?", collection.getId().toString());
    }

}
