package com.dedicatedcode.tmanager.model;

import org.jaudiotagger.audio.AudioFile;
import org.jaudiotagger.audio.AudioFileIO;
import org.jaudiotagger.audio.exceptions.CannotReadException;
import org.jaudiotagger.audio.exceptions.InvalidAudioFrameException;
import org.jaudiotagger.audio.exceptions.ReadOnlyFileException;
import org.jaudiotagger.tag.TagException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;

@Service
public class MediaCodecValidator {

    private static final Logger log = LoggerFactory.getLogger(MediaCodecValidator.class);
    private final MediaCodecProperties properties;

    public MediaCodecValidator(MediaCodecProperties properties) {
        this.properties = properties;
    }

    public boolean needsReEncoding(File file) throws IOException {
        try {
            AudioFile read = AudioFileIO.read(file);
            String currentFormat = read.getAudioHeader().getFormat();
            if (!currentFormat.equalsIgnoreCase(properties.getDesiredFormat())) {
                log.debug("Audio format needs encoding. Desired format: [{}] Actual Format: [{}]", properties.getDesiredFormat(), currentFormat);
                return true;
            }
            double size = ((double)file.length()) / 1000 / 1000;
            double durationInMinutes = ((double) read.getAudioHeader().getTrackLength()) / 60;
            double sizePerMinute = size / durationInMinutes;
            if (sizePerMinute > properties.getMaxSizePerMinute()) {
                log.debug("Audio format needs encoding. Desired max size per minute: [{}] Actual size per minute: [{}]", properties.getMaxSizePerMinute(), sizePerMinute);
                return true;
            }
            int amountOfChannels = read.getAudioHeader().getChannels().equalsIgnoreCase("Mono") ? 1 : 2;
            if (amountOfChannels > properties.getMaxChannels()) {
                log.debug("Audio format needs encoding. Desired max channels: [{}] Actual channels: [{}]", properties.getMaxChannels(), amountOfChannels);
                return true;
            }
            return false;
        } catch (TagException | ReadOnlyFileException | InvalidAudioFrameException | CannotReadException e) {
            throw new IOException(e);
        }
    }
}
