package com.dedicatedcode.tmanager.model;

import com.dedicatedcode.tmanager.MediaView;
import com.dedicatedcode.tmanager.model.tag.Tag;
import org.apache.commons.lang3.time.DurationFormatUtils;

import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

public final class MediaCollectionView extends IdAndNameView {
    private final List<MediaView> mediaList;
    private final List<TagView> connectedTags;
    private final boolean system;
    private final String duration;

    public MediaCollectionView(UUID id, String name, List<MediaView> mediaList, List<TagView> connectedTags, boolean system) {
        super(id.toString(), name);
        this.mediaList = mediaList;
        this.connectedTags = connectedTags;
        this.system = system;

        Duration durationTIme = Duration.of(mediaList.stream().map(m -> m.getMedia().getDuration()).reduce(Long::sum).orElse(0L), ChronoUnit.SECONDS);
        this.duration = DurationFormatUtils.formatDuration(durationTIme.toMillis(), "mm:ss");
    }

    public String getDuration() {
        return duration;
    }

    public List<MediaView> getMediaList() {
        return mediaList;
    }

    public List<TagView> getConnectedTags() {
        return connectedTags;
    }

    public boolean isSystem() {
        return system;
    }
}
