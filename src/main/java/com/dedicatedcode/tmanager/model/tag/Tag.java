package com.dedicatedcode.tmanager.model.tag;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;

public class Tag {
    private final String identifier;

    private final String name;
    private final List<UUID> collectionIds;

    Tag(String identifier, String name, List<UUID> collectionIds) {
        this.identifier = identifier;
        this.name = name;
        this.collectionIds = collectionIds;
    }

    public String getIdentifier() {
        return identifier;
    }

    public List<UUID> getCollectionIds() {
        return collectionIds;
    }

    public Optional<String> getName() {
        return Optional.ofNullable(this.name);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tag tag = (Tag) o;
        return identifier.equals(tag.identifier);
    }

    @Override
    public int hashCode() {
        return Objects.hash(identifier);
    }

    public Tag withName(String name) {
        return new Tag(identifier, name, collectionIds);
    }

    public Tag withConnectedCollections(List<UUID> collectionIds) {
        return new Tag(identifier, name, collectionIds);
    }
}
