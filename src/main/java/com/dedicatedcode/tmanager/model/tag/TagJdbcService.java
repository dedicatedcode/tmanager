package com.dedicatedcode.tmanager.model.tag;

import com.dedicatedcode.tmanager.model.collection.MediaCollection;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

@Service
public class TagJdbcService {
    private final JdbcTemplate jdbcTemplate;

    public TagJdbcService(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public List<Tag> getAll() {
        return this.jdbcTemplate.query("SELECT t.*, group_concat(ct.collection_id) as collection_ids FROM tag t LEFT JOIN collections_tags ct on t.tag_identifier = ct.tag_id GROUP BY t.tag_identifier", this::mapTag);
    }

    public List<Tag> getTagsFor(MediaCollection collection) {
        return this.jdbcTemplate.query("SELECT t.*, group_concat(ct.collection_id) as collection_ids FROM tag t LEFT JOIN collections_tags ct on t.tag_identifier = ct.tag_id WHERE ct.collection_id = ? GROUP BY t.tag_identifier;", this::mapTag, collection.getId().toString());

    }

    public Optional<Tag> getTagFor(String identifier) {
        List<Tag> tags = this.jdbcTemplate.query("SELECT t.*, group_concat(ct.collection_id) as collection_ids FROM tag t LEFT JOIN collections_tags ct on t.tag_identifier = ct.tag_id WHERE t.tag_identifier = ? GROUP BY t.tag_identifier;", this::mapTag, identifier);
        return tags.stream().findFirst();
    }

    private Tag mapTag(ResultSet rs, int rowNum) throws SQLException {
        List<UUID> collectionIds = new ArrayList<>();
        if (rs.getString("collection_ids") != null) {
            collectionIds.addAll(Arrays.stream(rs.getString("collection_ids").split(",")).map(UUID::fromString).toList());
        }

        return new Tag(rs.getString("tag_identifier"), rs.getString("name"), collectionIds);
    }

    public Tag create(String identifier) {
        this.jdbcTemplate.update("INSERT INTO tag (tag_identifier, name) VALUES (?, NULL)", identifier);
        return new Tag(identifier, null, null);
    }

    public void update(Tag tag) {
        this.jdbcTemplate.update("UPDATE tag SET  name = ? WHERE tag_identifier = ?",
                tag.getName().orElse(null),
                tag.getIdentifier());

        this.jdbcTemplate.update("DELETE FROM collections_tags WHERE tag_id = ?", tag.getIdentifier());
        tag.getCollectionIds().forEach(c -> this.jdbcTemplate.update("INSERT INTO collections_tags (collection_id, tag_id) VALUES (?,?)", c.toString(), tag.getIdentifier()));
    }

    public void delete(Tag tag) {
        this.jdbcTemplate.update("DELETE FROM collections_tags WHERE tag_id = ?", tag.getIdentifier());
        this.jdbcTemplate.update("DELETE FROM tag WHERE tag_identifier = ?", tag.getIdentifier());
    }
}
