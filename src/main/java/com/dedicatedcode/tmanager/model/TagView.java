package com.dedicatedcode.tmanager.model;

import java.util.List;

public class TagView extends IdAndNameView {

    private final List<IdAndNameView> connectedCollections;

    private final String duration;

    public TagView(String identifier, String name, List<IdAndNameView> connectedCollections, String duration) {
        super(identifier, name);
        this.connectedCollections = connectedCollections;
        this.duration = duration;
    }

    public TagView(String identifier, String name, List<IdAndNameView> connectedCollections) {
        this(identifier, name, connectedCollections, null);
    }
    public List<IdAndNameView> getConnectedCollections() {
        return connectedCollections;
    }

    public String getDuration() {
        return duration;
    }
}
