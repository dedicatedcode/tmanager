package com.dedicatedcode.tmanager.model;

import java.util.Objects;
import java.util.UUID;

public final class Media {
    private final UUID id;
    private final String name;
    private final String fileLocation;
    private final long duration;

    public Media(UUID id, String name, String fileLocation, long duration) {
        this.id = id;
        this.name = name;
        this.fileLocation = fileLocation;
        this.duration = duration;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Media media = (Media) o;
        return id.equals(media.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public UUID getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getFileLocation() {
        return fileLocation;
    }

    public long getDuration() {
        return duration;
    }

    @Override
    public String toString() {
        return "Media[" +
                "id=" + id + ", " +
                "name=" + name + ", " +
                "fileLocation=" + fileLocation + ", " +
                "duration=" + duration + ']';
    }


    public Media withName(String name) {
        return new Media(id, name, fileLocation, duration);
    }

    public Media withFileLocation(String fileLocation) {
        return new Media(id, name, fileLocation, duration);
    }
}
