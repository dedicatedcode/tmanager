package com.dedicatedcode.tmanager;

import com.dedicatedcode.tmanager.model.IdAndNameView;
import com.dedicatedcode.tmanager.model.Media;
import org.apache.commons.lang3.time.DurationFormatUtils;

import java.time.Duration;
import java.time.temporal.ChronoUnit;

public class MediaView extends IdAndNameView {
    private final Media media;
    private final String duration;

    public MediaView(Media media) {
        super(media.getId().toString(), media.getName());
        this.media = media;
        Duration durationTIme = Duration.of(media.getDuration(), ChronoUnit.SECONDS);
        this.duration = DurationFormatUtils.formatDuration(durationTIme.toMillis(), "mm:ss");
    }

    public Media getMedia() {
        return media;
    }

    public String getDuration() {
        return duration;
    }
}
