package com.dedicatedcode.tmanager;

import com.dedicatedcode.tmanager.model.collection.MediaCollection;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.RejectedExecutionException;

public interface UploadManager {
    String schedule(List<MediaCollection> mediaCollection) throws RejectedExecutionException;

    Optional<UploadStatus> getStatus(String uploadIdentifier);

    record UploadStatus(com.dedicatedcode.tmanager.model.upload.JobState state) {}
}
