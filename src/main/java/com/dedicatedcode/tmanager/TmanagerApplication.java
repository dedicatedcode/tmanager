package com.dedicatedcode.tmanager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@EnableConfigurationProperties
@SpringBootApplication
public class TmanagerApplication {

	public static void main(String[] args) {
		SpringApplication.run(TmanagerApplication.class, args);
	}

}
