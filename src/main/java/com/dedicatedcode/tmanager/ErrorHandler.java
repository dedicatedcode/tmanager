package com.dedicatedcode.tmanager;

import com.dedicatedcode.tmanager.model.Error;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ErrorHandler {

    @ExceptionHandler(ImportException.class)
    public Error handleImportException(ImportException importException) {
        return new Error();
    }
}
