package com.dedicatedcode.tmanager.controller;

import com.dedicatedcode.tmanager.ImportException;
import com.dedicatedcode.tmanager.Reencoder;
import com.dedicatedcode.tmanager.StorageService;
import com.dedicatedcode.tmanager.model.Media;
import com.dedicatedcode.tmanager.model.MediaCodecValidator;
import com.dedicatedcode.tmanager.model.collection.MediaCollection;
import com.dedicatedcode.tmanager.model.collection.MediaJdbcService;
import org.jaudiotagger.audio.AudioFile;
import org.jaudiotagger.audio.AudioFileIO;
import org.jaudiotagger.audio.exceptions.CannotReadException;
import org.jaudiotagger.audio.exceptions.InvalidAudioFrameException;
import org.jaudiotagger.audio.exceptions.ReadOnlyFileException;
import org.jaudiotagger.tag.FieldKey;
import org.jaudiotagger.tag.TagException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.UUID;

@Controller
public class FileController {
    private static final Logger log = LoggerFactory.getLogger(FileController.class);

    private final StorageService storageService;
    private final MediaJdbcService mediaJdbcService;
    private final Reencoder reencoder;
    private final MediaCodecValidator mediaCodecValidator;
    private final int minimumDurationForCollectionCreation;

    public FileController(StorageService storageService, MediaJdbcService mediaJdbcService, Reencoder reencoder, MediaCodecValidator mediaCodecValidator, @Value("${automation.create.collection.threshold-in-minutes}") int minimumDurationForCollectionCreation) {
        this.storageService = storageService;
        this.mediaJdbcService = mediaJdbcService;
        this.reencoder = reencoder;
        this.mediaCodecValidator = mediaCodecValidator;
        this.minimumDurationForCollectionCreation = minimumDurationForCollectionCreation;
    }

    @PostMapping(value = "/upload/{collectionId}", produces = "application/json", consumes = {MediaType.MULTIPART_FORM_DATA_VALUE})
    public ResponseEntity<Void> uploadIntoCollection(@PathVariable UUID collectionId, @RequestPart(name = "files[]") MultipartFile file) {
        String originalFilename = Optional.ofNullable(file.getOriginalFilename()).orElse(UUID.randomUUID().toString());
        log.info("processing new binary upload for [{}]", originalFilename);
        try {
            String extension = originalFilename.substring(originalFilename.lastIndexOf("."));
            Optional<File> writtenFile = this.storageService.writeToTmp(UUID.randomUUID() + extension, file.getInputStream());
            writtenFile
                    .map(f -> {
                        try {
                            if (mediaCodecValidator.needsReEncoding(f)) {
                                File reencoded = reencoder.reencode(f);
                                if (mediaCodecValidator.needsReEncoding(reencoded)) {
                                    log.warn("Re-encoded file [{}] failed validation check. This will lead to additional re-encoding. Please match your encoding settings with your validation settings.", f.getName());
                                }
                                return reencoded;
                            } else {
                                return f;
                            }
                        } catch (IOException e) {
                            throw new RuntimeException(e);
                        }
                    })
                    .ifPresent(f -> {
                        log.debug("successfully copied incoming audio file to [{}]", f);
                        try {
                            AudioFile af = AudioFileIO.read(f);
                            MediaCollection collection;
                            if (collectionId == null) {
                                Optional<String> collectionName = extractCollectionName(f, af);
                                collection = collectionName.map(cname -> {
                                    Optional<MediaCollection> optCollection = this.mediaJdbcService.getCollectionBy(cname);
                                    return optCollection.orElseGet(() -> {
                                        log.debug("Unknown collection [{}] found, will create it.", cname);
                                        MediaCollection mediaCollection = new MediaCollection(UUID.randomUUID(), cname, Collections.emptyList());
                                        this.mediaJdbcService.create(mediaCollection);
                                        return mediaCollection;
                                    });
                                }).orElse(this.mediaJdbcService.getDefaultCollection());
                            } else {
                                collection = this.mediaJdbcService.getCollectionBy(collectionId).orElseThrow(NoSuchElementException::new);
                            }

                            String name = extractMediaName(f, af).orElse(originalFilename);
                            String finalLocation = collection.getId() + File.separator + f.getName();
                            File newLocation = this.storageService.move(f, finalLocation).orElseThrow(ImportException::new);
                            Media media = new Media(UUID.randomUUID(), name, newLocation.getAbsolutePath(), af.getAudioHeader().getTrackLength());
                            this.mediaJdbcService.saveMedia(collection.append(media), media);
                        } catch (CannotReadException | IOException | TagException | ReadOnlyFileException |
                                 InvalidAudioFrameException e) {
                            throw new RuntimeException(e);
                        }
                    });

        } catch (IOException e) {
            log.error("Could not write tmp file");
            throw new RuntimeException(e);
        }
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @PostMapping(value = "/upload", produces = "application/json", consumes = {MediaType.MULTIPART_FORM_DATA_VALUE})
    public ResponseEntity<Void> upload(@RequestPart(name = "files[]") MultipartFile file) {
        return uploadIntoCollection(null, file);
    }

    private Optional<String> extractMediaName(File file, AudioFile af) {
        String name = getTag(af, FieldKey.TITLE);
        return Optional.ofNullable(name);
    }

    private Optional<String> extractCollectionName(File f, AudioFile af) {
        if (af.getAudioHeader().getTrackLength() > minimumDurationForCollectionCreation * 60) {
            return Optional.ofNullable(getTag(af, FieldKey.TITLE));
        }
        String collection = getTag(af, FieldKey.ALBUM);
        if (collection != null && !collection.isBlank()) {
            log.debug("detected collection [{}] for [{}]", collection, f);
            return Optional.of(collection);
        } else {
            log.debug("using default collection [{}] for [{}] since no tag was available", collection, f);
            return Optional.empty();
        }
    }

    private static String getTag(AudioFile af, FieldKey tagName) {
        String tagValue = af.getTag() != null ? af.getTag().getFirst(tagName) : null;
        if (tagValue != null && !tagValue.isBlank()) {
            return tagValue;
        } else {
            return null;
        }
    }

    @RequestMapping(value = "/download/{name}")
    public ResponseEntity<InputStreamResource> download(@PathVariable String name) {
        return Optional.ofNullable(getClass().getResourceAsStream("/static/" + name))
                .map(is -> new ResponseEntity<>(new InputStreamResource(is), HttpStatus.OK))
                .orElse(ResponseEntity.notFound().build());
    }
}
