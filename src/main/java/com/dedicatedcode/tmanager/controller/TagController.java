package com.dedicatedcode.tmanager.controller;

import com.dedicatedcode.tmanager.UploadManager;
import com.dedicatedcode.tmanager.model.collection.MediaCollection;
import com.dedicatedcode.tmanager.model.collection.MediaJdbcService;
import com.dedicatedcode.tmanager.model.tag.Tag;
import com.dedicatedcode.tmanager.model.tag.TagJdbcService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.RejectedExecutionException;

@RestController
public class TagController {
    private static final Logger log = LoggerFactory.getLogger(TagController.class);
    private final TagJdbcService tagJdbcService;
    private final MediaJdbcService mediaJdbcService;

    private final UploadManager uploadManager;

    private final ExecutorService executor = Executors.newSingleThreadExecutor();

    public TagController(TagJdbcService tagJdbcService, MediaJdbcService mediaJdbcService, UploadManager uploadManager) {
        this.tagJdbcService = tagJdbcService;
        this.mediaJdbcService = mediaJdbcService;
        this.uploadManager = uploadManager;
    }

    @GetMapping("/trigger/{tagId}")
    public SelectResponse triggerTag(@PathVariable String tagId) {
        Optional<Tag> tag = this.tagJdbcService.getTagFor(tagId);
        if (tag.isEmpty()) {
            log.info("new tag seen with identifier [{}]. Will store it in the database.", tagId);
            this.tagJdbcService.create(tagId);
            return new SelectResponse(Status.UNKNOWN_TAG);
        } else {
            List<MediaCollection> collections = this.mediaJdbcService.getCollectionsBy(tag.get());
            if (collections.isEmpty()) {
                return new SelectResponse(Status.NO_COLLECTION_CONFIGURED);
            } else {
                try {
                    String jobId = uploadManager.schedule(collections);
                    return new JobSelectResponse(Status.STARTED, jobId);
                } catch (RejectedExecutionException e) {
                    log.info("upload is rejected because there is already a running upload job");
                    return new SelectResponse(Status.BUSY);
                }
            }
        }
    }

    private enum Status {
        STARTED,
        UNKNOWN_TAG,
        NO_COLLECTION_CONFIGURED,
        BUSY
    }

    public static class SelectResponse {
        private final Status status;

        public SelectResponse(Status status) {
            this.status = status;
        }

        public Status getStatus() {
            return status;
        }

        @Override
        public String toString() {
            return "SelectResponse[" +
                    "status=" + status + ']';
        }
    }

    public static final class JobSelectResponse extends SelectResponse {

        private final String job;

        public JobSelectResponse(Status status, String job) {
            super(status);
            this.job = job;
        }

        public String getJob() {
            return job;
        }
    }
}
