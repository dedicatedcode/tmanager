package com.dedicatedcode.tmanager.controller;

import com.dedicatedcode.tmanager.MediaView;
import com.dedicatedcode.tmanager.StorageService;
import com.dedicatedcode.tmanager.model.IdAndNameView;
import com.dedicatedcode.tmanager.model.Media;
import com.dedicatedcode.tmanager.model.MediaCollectionView;
import com.dedicatedcode.tmanager.model.TagView;
import com.dedicatedcode.tmanager.model.collection.MediaCollection;
import com.dedicatedcode.tmanager.model.collection.MediaJdbcService;
import com.dedicatedcode.tmanager.model.tag.Tag;
import com.dedicatedcode.tmanager.model.tag.TagJdbcService;
import org.apache.commons.lang3.time.DurationFormatUtils;
import org.apache.logging.log4j.util.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.io.File;
import java.time.Duration;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Controller
public class WebController {

    private static final Logger log = LoggerFactory.getLogger(WebController.class);

    private final MediaJdbcService mediaJdbcService;

    private final TagJdbcService tagJdbcService;

    private final StorageService storageService;


    public WebController(MediaJdbcService mediaJdbcService, TagJdbcService tagJdbcService, StorageService storageService) {
        this.mediaJdbcService = mediaJdbcService;
        this.tagJdbcService = tagJdbcService;
        this.storageService = storageService;
    }


    @GetMapping(value = {"", "/"})
    public String loadStart(ModelMap model) {
        model.addAttribute("page", "HOME");
        return "index";
    }

    @GetMapping("/collections.html")
    public String loadCollectionsPage(ModelMap model) {
        model.addAttribute("page", "COLLECTIONS");
        List<MediaCollection> collections = mediaJdbcService.getAllCollections();
        model.addAttribute("collections", collections.stream()
                .map(c -> new MediaCollectionView(c.getId(), c.getName(), c.getMediaList().stream().map(MediaView::new).toList(), tagJdbcService.getTagsFor(c).stream()
                        .map(t -> mapTag(t, collections.stream().collect(Collectors.toMap(MediaCollection::getId, Function.identity())), true)).toList(), c.isSystem())));
        return "collections";
    }

    @GetMapping("/collections/{collectionId}.html")
    public String editCollectionPage(ModelMap model, @PathVariable UUID collectionId) {
        model.addAttribute("page", "COLLECTIONS");
        MediaCollection collection = this.mediaJdbcService.getCollectionBy(collectionId).orElseThrow(NoSuchElementException::new);

        List<TagView> selectedTags = this.tagJdbcService.getTagsFor(collection).stream().map(t -> new TagView(t.getIdentifier(), t.getName().orElse(null), null, null)).toList();
        model.addAttribute("collection", new MediaCollectionView(collection.getId(), collection.getName(), collection.getMediaList().stream().map(MediaView::new).toList(), selectedTags, collection.isSystem()));
        model.addAttribute("tags", this.tagJdbcService.getAll().stream().map(t -> mapTag(t, this.mediaJdbcService.getAllCollections().stream().collect(Collectors.toMap(MediaCollection::getId, Function.identity())), true)).toList());
        return "edit_collection";
    }

    @GetMapping("/collections/new.html")
    public String newCollectionPage(ModelMap model) {
        model.addAttribute("page", "COLLECTIONS");
        model.addAttribute("tags", this.tagJdbcService.getAll().stream().map(t -> mapTag(t, this.mediaJdbcService.getAllCollections().stream().collect(Collectors.toMap(MediaCollection::getId,Function.identity())), true)).toList());
        return "new_collection";
    }

    @PostMapping("/collections/new.html")
    public String newCollection(@RequestParam String name) {
        MediaCollection mediaCollection = new MediaCollection(UUID.randomUUID(), name, Collections.emptyList());

        this.mediaJdbcService.create(mediaCollection);
        return "redirect:/collections.html";
    }

    @PostMapping("/collections/{collectionId}.html")
    public String updateCollection(@PathVariable UUID collectionId, @RequestParam(name = "name") String name) {
        MediaCollection collection = this.mediaJdbcService.getCollectionBy(collectionId).orElseThrow(NoSuchElementException::new);
        collection = collection.withName(name);
        this.mediaJdbcService.update(collection);
        return "redirect:/collections.html";
    }

    @DeleteMapping(value = "/collections/{id}.html", produces = "application/json")
    @ResponseBody
    public DeleteResponse deleteCollection(@PathVariable UUID id) {
        MediaCollection collection = this.mediaJdbcService.getCollectionBy(id).orElseThrow(NoSuchElementException::new);
        MediaCollection defaultCollection = this.mediaJdbcService.getDefaultCollection();
        if (collection.equals(defaultCollection)) {
            throw new IllegalArgumentException("default collection must not be deleted!");
        }
        collection.getMediaList().forEach(m -> this.mediaJdbcService.saveMedia(defaultCollection, m));
        this.mediaJdbcService.delete(collection);
        return new DeleteResponse(200, "/collections.html");
    }


    @GetMapping("/tags.html")
    public String loadTagsPage(ModelMap model) {
        model.addAttribute("page", "TAGS");
        Map<UUID, MediaCollection> collections = mediaJdbcService.getAllCollections().stream().collect(Collectors.toMap(MediaCollection::getId, Function.identity()));
        model.addAttribute("tags", this.tagJdbcService.getAll().stream().map(t -> mapTag(t, collections, true)).toList());
        return "tags";
    }

    @GetMapping("/media/{id}.html")
    public String loadEditMediaPage(ModelMap model, @PathVariable UUID id) {
        model.addAttribute("page", "MEDIA");
        Media media = this.mediaJdbcService.getMedia(id).orElseThrow(NoSuchElementException::new);
        List<MediaCollection> collections = mediaJdbcService.getAllCollections();
        model.addAttribute("media", new MediaView(media));
        model.addAttribute("connectedCollection", collections.stream().filter(c -> c.getMediaList().contains(media)).findAny().orElse(null));
        model.addAttribute("collections", collections);
        return "edit_media";
    }

    @DeleteMapping(value = "/media/{id}.html", produces = "application/json")
    @ResponseBody
    public DeleteResponse deleteMedia(@PathVariable UUID id) {
        Media media = this.mediaJdbcService.getMedia(id).orElseThrow(NoSuchElementException::new);
        this.storageService.delete(media);
        this.mediaJdbcService.delete(media);
        return new DeleteResponse(200, "/collections.html");
    }

    @PostMapping("/media/{id}.html")
    public String updateMedia(@PathVariable UUID id, @RequestParam String name, @RequestParam(name = "selected-collection") UUID selectedCollectionId) {
        Media media = this.mediaJdbcService.getMedia(id).orElseThrow(NoSuchElementException::new);
        media = media.withName(name);
        MediaCollection newCollection = this.mediaJdbcService.getCollectionBy(selectedCollectionId).orElseThrow(NoSuchElementException::new);
        MediaCollection oldCollection = this.mediaJdbcService.getCollectionBy(media).orElseThrow(IllegalStateException::new);
        if (!oldCollection.equals(newCollection)) {
            log.debug("moving file since collection is changed now from [{}] to [{}]", oldCollection.getId(), newCollection.getId());
            media = media.withFileLocation(this.storageService.move(new File(media.getFileLocation()), newCollection.getId() + File.separator  + media.getFileLocation().substring(media.getFileLocation().lastIndexOf("/")+1)).orElseThrow(IllegalStateException::new).getAbsolutePath());
        }
        this.mediaJdbcService.saveMedia(newCollection, media);
        return "redirect:/collections.html";
    }

    @GetMapping("/tags/{tagIdentifier}.html")
    public String loadEditTagPage(ModelMap model, @PathVariable String tagIdentifier) {
        model.addAttribute("page", "TAGS");
        Tag tag = this.tagJdbcService.getTagFor(tagIdentifier).orElseThrow(NoSuchElementException::new);

        List<MediaCollection> collections = mediaJdbcService.getAllCollections();
        model.addAttribute("tag", mapTag(tag, collections.stream().collect(Collectors.toMap(MediaCollection::getId, Function.identity())), false));
        model.addAttribute("collections", collections.stream().map(c -> new IdAndNameView(c.getId().toString(), c.getName())).toList());
        return "edit_tag";
    }

    @PostMapping("/tags/{tagIdentifier}.html")
    public String updateTag(@PathVariable String tagIdentifier, @RequestParam(required = false) String name, @RequestParam(name = "selected-collections", required = false) List<UUID> selectedCollectionIds) {
        Tag tag = this.tagJdbcService.getTagFor(tagIdentifier).orElseThrow(NoSuchElementException::new);
        tag = tag.withName(Strings.isBlank(name) ? null : name).withConnectedCollections(selectedCollectionIds);

        this.tagJdbcService.update(tag);
        return "redirect:/tags.html";
    }

    @DeleteMapping(value = "/tags/{tagIdentifier}.html", produces = "application/json")
    @ResponseBody
    public DeleteResponse deleteTag(@PathVariable String tagIdentifier) {
        Tag tag = this.tagJdbcService.getTagFor(tagIdentifier).orElseThrow(NoSuchElementException::new);

        this.tagJdbcService.delete(tag);
        return new DeleteResponse(200, "/tags.html");
    }

    private TagView mapTag(Tag tag, Map<UUID, MediaCollection> collections, boolean withFallbackName) {
        List<MediaCollection> mediaCollections = tag.getCollectionIds().stream().map(collections::get).toList();

        List<IdAndNameView> collectionView = new ArrayList<>();
        Duration durationValue = Duration.ZERO;

        for (MediaCollection mediaCollection : mediaCollections) {
            collectionView.add(new IdAndNameView(mediaCollection.getId().toString(), mediaCollection.getName()));
            for (Media media : mediaCollection.getMediaList()) {
                durationValue = durationValue.plusSeconds(media.getDuration());
            }
        }
        String duration = DurationFormatUtils.formatDuration(durationValue.toMillis(), "mm:ss");
        if (withFallbackName) {
            return new TagView(tag.getIdentifier(), tag.getName().orElse(tag.getIdentifier()), collectionView, duration);
        } else {
            return new TagView(tag.getIdentifier(), tag.getName().orElse(null), collectionView, duration);
        }
    }

}
