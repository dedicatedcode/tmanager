package com.dedicatedcode.tmanager.controller;

import com.dedicatedcode.tmanager.UploadManager;
import com.dedicatedcode.tmanager.model.upload.JobState;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Objects;
import java.util.Optional;

@RestController
public class JobController {

    private final UploadManager uploadManager;

    public JobController(UploadManager uploadManager) {
        this.uploadManager = uploadManager;
    }

    @GetMapping(value = "/job/{jobId}", produces = "application/json")
    @ResponseBody
    public JobStateResponse getJobState(@PathVariable String jobId) {
        Optional<UploadManager.UploadStatus> status = this.uploadManager.getStatus(jobId);
        return status.map(s -> new JobStateResponse(s.state())).orElse(new JobStateResponse(JobResponseState.UNKNOWN));
    }

    private static final class JobStateResponse {
        private final JobResponseState status;

        private JobStateResponse(JobState state) {
            switch (state) {

                case FAILED -> {
                    status = JobResponseState.FAILED;
                }
                case RUNNING -> {
                    status = JobResponseState.RUNNING;
                }
                case CREATED -> {
                    status = JobResponseState.STARTED;
                }
                case FINISHED -> {
                    status = JobResponseState.FINISHED;
                }
                default -> throw new IllegalArgumentException("unknown enum constant");
            }
        }

        private JobStateResponse(JobResponseState status) {
            this.status = status;
        }

        public JobResponseState getStatus() {
            return status;
        }
    }

    private enum JobResponseState {
        STARTED,
        RUNNING,
        FAILED,
        FINISHED,
        UNKNOWN
    }
}
