package com.dedicatedcode.tmanager.controller;

public class DeleteResponse {
    private final int status;
    private final String returnUrl;

    public DeleteResponse(int status, String returnUrl) {
        this.status = status;
        this.returnUrl = returnUrl;
    }

    public int getStatus() {
        return status;
    }

    public String getReturnUrl() {
        return returnUrl;
    }
}
