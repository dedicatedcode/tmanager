package com.dedicatedcode.tmanager;

import java.io.File;

public interface Reencoder {
    File reencode(File file);
}
