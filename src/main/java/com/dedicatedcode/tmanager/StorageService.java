package com.dedicatedcode.tmanager;

import com.dedicatedcode.tmanager.model.Media;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.FileCopyUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.Optional;

@Service
public class StorageService {
    private static final Logger log = LoggerFactory.getLogger(StorageService.class);
    private final File mediaFolder;
    private final File mediaTmpFolder;


    public StorageService(@Value("${storage.media.location}") String mediaLocation, @Value("${storage.media.tmp-location}") String tmpMediaLocation) {
        this.mediaFolder = new File(mediaLocation);
        if (mediaFolder.mkdirs()) {
            log.info("create media storage folder under [{}]", mediaFolder.getAbsolutePath());
        }
        this.mediaTmpFolder = new File(tmpMediaLocation);
        if (mediaTmpFolder.mkdirs()) {
            log.info("create temporary media storage folder under [{}]", mediaTmpFolder.getAbsolutePath());
        }
    }

    public Optional<File> writeToTmp(String name, InputStream inputStream) {
        File targetFile = new File(mediaTmpFolder, name);
        try {
            FileCopyUtils.copy(inputStream, new FileOutputStream(targetFile));
            return Optional.of(targetFile);
        } catch (IOException e) {
            log.error("could not write to file [{}]", targetFile, e);
        }
        return Optional.empty();
    }

    public Optional<File> move(File file, String newLocation) {
        File newParent = new File(mediaFolder, newLocation);
        newParent.getParentFile().mkdirs();
        if (newParent.getParentFile().exists()) {
            try {
                Files.move(file.toPath(), newParent.toPath(), StandardCopyOption.REPLACE_EXISTING);
                return Optional.of(newParent);
            } catch (IOException e) {
                log.error("could not move file [{}] to new location [{}]", file, newParent);
            }
        }
        return Optional.empty();
    }

    public void delete(Media media) {
        new File(media.getFileLocation()).delete();
    }
}
