CREATE TABLE collection
(
    id         VARCHAR(36)   NOT NULL PRIMARY KEY,
    name       VARCHAR(1024) NOT NULL UNIQUE,
    is_default BOOLEAN DEFAULT FALSE
);

CREATE TABLE media
(
    id            VARCHAR(36)   NOT NULL PRIMARY KEY,
    name          VARCHAR(1024) NOT NULL,
    storage_path  VARCHAR(1024) NOT NULL,
    collection_id VARCHAR(36),
    sort_order    INTEGER       NOT NULL,
    duration      INTEGER       NOT NULL,
    FOREIGN KEY (collection_id) REFERENCES collection (id)
);

CREATE TABLE tag
(
    tag_identifier VARCHAR(1024) NOT NULL,
    name VARCHAR(1024),
    collection_id VARCHAR(36),
    FOREIGN KEY (collection_id) REFERENCES collection (id)
);

INSERT INTO collection VALUES ('82254fa8-1e0d-4551-a37b-1c0aeac73e80', 'DEFAULT', true);