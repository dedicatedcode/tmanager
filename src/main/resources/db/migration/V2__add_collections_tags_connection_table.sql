CREATE TABLE collections_tags
(
    collection_id VARCHAR(36)   NOT NULL,
    tag_id        VARCHAR(36)   NOT NULL,
    FOREIGN KEY (collection_id) REFERENCES collection (id),
    FOREIGN KEY (tag_id) REFERENCES tag (tag_identifier)
);

INSERT INTO collections_tags(collection_id, tag_id) SELECT collection_id, tag_identifier FROM tag WHERE collection_id IS NOT NULL;

ALTER TABLE tag RENAME TO _tag;


CREATE TABLE tag
(
    tag_identifier VARCHAR(1024) NOT NULL,
    name VARCHAR(1024)
);

INSERT INTO tag(tag_identifier, name)  SELECT tag_identifier, name FROM _tag;

