FROM ubuntu:22.04
ENV LANG C.UTF-8
RUN apt-get update -q
RUN DEBIAN_FRONTEND="noninteractive" apt-get install -qy openjdk-21-jre-headless ffmpeg
ENV spring_profiles_active=docker

EXPOSE 8080

ARG JAR_FILE=target/app.jar

ADD ${JAR_FILE} app.jar
ADD docker/start.sh start.sh
RUN chmod u+x start.sh
RUN useradd --uid 9876 --user-group --no-create-home --no-log-init --shell /bin/bash app

ENTRYPOINT ["bash", "/start.sh"]
