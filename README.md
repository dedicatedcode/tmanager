# Introduction

A small application to manage updating a Kreativ-Tonie through an external trigger.
My current use case for this is:
- I set up a microcontroller with an NFC reader.
- We choose a NFC card, place it on the reader and the microcontroller sends a http request to this application
- based on the trigger and the configuration, the application updates the Kreativ-Tonie, and we can start listening to our favorite audiobook.

## Build

```
./mvnw clean install
```

## Installation

### Docker-Compose
Create a file `docker-compose.yml` and paste the next block into it. 
Add your Toni-Id, your username and password. 

To obtain your tonie-id got to https://my.tonies.com/creative-tonies/.
Select your Kreativ-Tonie you want to user. The id is now shown in the URL of you browser. It is the whole string after https://my.tonies.com/creative-tonies/.


```yaml
version: "3.5"

services:
  app:
    image: registry.gitlab.com/dedicatedcode/tmanager:latest
    environment:
      - TONIE_ID=
      - MY_TONIE_USERNAME=
      - MY_TONIE_PASSWORD=
      - REENCODE_CONTENT=true
    volumes:
      - <location where the db is stored>:/storage/database
      - <location where the audiofiles are>:/storage/data
```

Now you can start the app by executing:
```docker-compose up```



